package net.accenture.pl;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
	
	@RequestMapping("/users")
	public String usersHome(){
		return "/users/home";
	}

}
